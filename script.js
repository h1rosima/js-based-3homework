let entAge = prompt("Введите свой возраст: ");
if (entAge < 12) {
    alert("Ты ребенок.")
} else if (entAge < 18) {
    alert("Ты подросток.")
} else {
    alert("Вы взрослый.")
}


//////////

let entMonth = prompt("Введіть назву місяця (українською мовою):").toLowerCase();

let daysInMonth;

switch (entMonth) {
    case "січень":
    case "березень":
    case "травень":
    case "липень":
    case "серпень":
    case "жовтень":
    case "грудень":
        daysInMonth = 31;
        break;
    case "квітень":
    case "червень":
    case "вересень":
    case "листопад":
        daysInMonth = 30;
        break;
    case "лютий":
        daysInMonth = 28;
        break;
    default:
        console.log("Ви ввели некоректний місяць.");
        break;
}

if (daysInMonth !== undefined) {
    console.log(`У місяці ${entMonth} ${daysInMonth} днів.`);
}


